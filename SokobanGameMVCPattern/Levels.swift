import Foundation
import UIKit
import Socket

class Levels {
//    MARK: - Peoperties
    private var level: Int
    private var prefixFileName: String
    private var endFileName: String
    
//    MARK: - Initializer
    public init() {
        level = 1
        prefixFileName = "levels/level"
        endFileName = ".sok"
    }
    //    MARK: - Getters and setters
    public func getLevel() -> Int {
        return (level - 1)
    }
    
    //    MARK: - Public methods
    public func nextLevel() -> [[Int]] {
        var desktop: [[Int]]
        
        switch level {
        case 1:
            desktop = getFirstLevel()
            break
        case 2:
            desktop = getSecondLevel()
            break
        case 3:
            desktop = getThirdLevel()
            break
        case 4, 5, 6:
            desktop = loadLevelFromFile(prefixFileName + String(level) + endFileName)
            break
        case 7, 8, 9:
            desktop = loadFromServer(level)
            break
        default:
            level = 1
            desktop = getFirstLevel()
        }
        level = level + 1
        return desktop
    }
    
    //    MARK: - Helpers
    private func getFirstLevel() -> [[Int]] {
        let array: [[Int]] =  [
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [2, 0, 0, 0, 0, 0, 0, 0, 0, 2],
            [2, 0, 0, 0, 0, 0, 0, 3, 4, 2],
            [2, 0, 0, 0, 0, 0, 0, 0, 0, 2],
            [2, 0, 0, 1, 0, 0, 0, 0, 0, 2],
            [2, 0, 0, 0, 0, 0, 0, 0, 0, 2],
            [2, 0, 0, 0, 0, 0, 0, 0, 0, 2],
            [2, 0, 0, 0, 0, 0, 0, 0, 0, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
        ]
        return array
    }
    
    private func getSecondLevel() -> [[Int]] {
        let array: [[Int]] =  [
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
            [2, 0, 0, 4, 4, 0, 0, 0, 0, 2],
            [2, 0, 0, 3, 3, 0, 0, 0, 0, 2],
            [2, 0, 0, 0, 0, 0, 0, 0, 0, 2],
            [2, 0, 0, 1, 0, 0, 0, 0, 0, 2],
            [2, 0, 0, 0, 0, 0, 0, 0, 0, 2],
            [2, 0, 0, 0, 0, 0, 0, 0, 0, 2],
            [2, 0, 0, 0, 0, 0, 0, 0, 0, 2],
            [2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
        ]
        return array
    }
    
    private func getThirdLevel() -> [[Int]] {
        let array: [[Int]] =  [
            [0, 0, 2, 2, 2, 2, 2, 0, 0, 0],
            [2, 2, 2, 0, 0, 0, 2, 0, 0, 0],
            [2, 0, 1, 0, 0, 0, 2, 0, 0, 0],
            [2, 2, 2, 0, 3, 4, 2, 0, 0, 0],
            [2, 4, 2, 2, 3, 0, 2, 0, 0, 0],
            [2, 0, 2, 0, 4, 0, 2, 2, 0, 0],
            [2, 3, 0, 0, 3, 3, 4, 2, 0, 0],
            [2, 0, 0, 0, 4, 0, 0, 2, 0, 0],
            [2, 2, 2, 2, 2, 2, 2, 2, 0, 0]
        ]
        return array
    }
    
    private func loadLevelFromFile(_ fileName: String) -> [[Int]]{
        
        if let filePath = URL(string: fileName, relativeTo: Bundle.main.bundleURL) {
            do {
                let handle = try FileHandle(forReadingFrom: filePath)

                defer {
                    try? handle.close()
                }
                
                var data = try handle.read(upToCount: 1)
                
                let filePathResourceValues = try filePath.resourceValues(forKeys: [.fileSizeKey])
                let fileSize = filePathResourceValues.fileSize
                
                var array: [Character] = Array(repeating: "n", count: fileSize!)
                
                var index: Int = 0
                while data != nil && !data!.isEmpty {
                    let symbol: Character = Character(UnicodeScalar(data![0]))
                    if ("0" <= symbol && symbol <= "4") {
                        array[index] = symbol
                        index = index + 1
                    } else if (symbol == "\n") {
                        array[index] = "A"
                        index = index + 1
                    }
                    data = try handle.read(upToCount: 1)
                }
                
                if (array[index] != "\n") {
                    array[index] = "A"
                    index = index + 1
                }
                
                let text = String(array.prefix(index))
                
                array.removeAll()
                
                return(convert(text))
            } catch {
                print("contents could not be loaded")
            }
        } else {
            print("\(fileName) was not found")
        }
        return []
    }
    
    private func loadFromServer(_ level: Int) -> [[Int]] {
        var result: [[Int]] = .init()
        do {
            let socket = try Socket.create()
            try socket.connect(to: "194.152.37.7", port: 5543, timeout: 1000)
            try socket.write(from: "level\(level)")
            if let string = try socket.readString() {
                result = convert(string)
            }
            socket.close()
        } catch {
            return []
        }
        return result
    }
    
    private func convert(_ text: String) -> [[Int]] {
        var row: Int = 0
        for char in text {
            let symbol: Character = char
            if symbol == "A" {
                row = row + 1
            }
        }
        
        var array: [[Int]] = Array(repeating: [Int](), count: row)
        
        var column: Int = 0
        var indexRow: Int = 0
        
        for char in text {
            let symbol: Character = char
            if (symbol == "A") {
                array[indexRow] = Array(repeating: 0, count: column)
                indexRow = indexRow + 1
                column = 0
            } else {
                column = column + 1
            }
        }
        
        row = 0
        column = 0
        
        for char in text {
            let symbol: Character = char
            if (symbol == "A") {
                row = row + 1
                column = 0
            } else {
                let number: Int = Int(String(symbol))!
                array[row][column] = number
                column = column + 1
            }
        }
        
        return array
    }
}
