import UIKit

class Canvas: UIView {
    
//    MARK: - Properties
    private let model: Model
    private var imageGamer: UIImage?
    private var imageWall: UIImage?
    private var imageBox: UIImage?
    private var imageGoal: UIImage?
    private var imageGround: UIImage?
    private var imageError: UIImage?
    
    
//    MARK: - Initializers
    public init(_ model: Model) {
        self.model = model
        imageGamer = UIImage(named: "myGamer.png")
        imageWall = UIImage(named: "wall.png")
        imageBox = UIImage(named: "box.png")
        imageGoal = UIImage(named: "goal.png")
        imageGround = UIImage(named: "ground.png")
        imageError = UIImage(named: "system_error.png")
        super.init(frame: .zero)
        backgroundColor = .lightGray
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    MARK: - Public methods
    func update() {
        setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if (model.getStateModel()) {
            drawDesktop()
        } else {
            drawErrorGame()
        }
    }
    
//    MARK: - Helpers
    private func drawDesktop() {
        let desktop: [[Int]] = model.getDesktop()
        
        let widthDesktop = CGFloat(frame.width/CGFloat(desktop[0].count+1))
        let heightDesktop = CGFloat(widthDesktop)
        let start = CGFloat(widthDesktop/2)
        let offset = 0.0
        var xDesktop = CGFloat(start)
        var yDesktop = 50.0
        
        for row in desktop {
            for element in row {
                if element == 0 {
                    imageGround?.draw(in: CGRect(x: xDesktop, y: yDesktop, width: widthDesktop, height: heightDesktop))
                } else if element == 1 {
                    imageGamer?.draw(in: CGRect(x: xDesktop, y: yDesktop, width: widthDesktop, height: heightDesktop))
                } else if element == 2 {
                    imageWall?.draw(in: CGRect(x: xDesktop, y: yDesktop, width: widthDesktop, height: heightDesktop))
                } else if element == 3 {
                    imageBox?.draw(in: CGRect(x: xDesktop, y: yDesktop, width: widthDesktop, height: heightDesktop))
                } else if element == 4 {
                    imageGoal?.draw(in: CGRect(x: xDesktop, y: yDesktop, width: widthDesktop, height: heightDesktop))
                }
                xDesktop = xDesktop + widthDesktop + offset
                }
            yDesktop = yDesktop + heightDesktop + offset
            xDesktop = start
        }
    }
    
    func drawErrorGame() {
        let widthDesktop = CGFloat(frame.width)
        imageError?.draw(in: CGRect(x: 0, y: 0, width: widthDesktop, height: 300))
    }
}
