import Foundation

class FileReader {
    @available(macOS 10.15.4, *)
    public func loadLevelFromFile(_ level: String) throws -> String {
        
        if let filePath = Bundle.module.url(forResource: level, withExtension: "sok") {
            
            do {
                let handle = try FileHandle(forReadingFrom: filePath)

                defer {
                    try? handle.close()
                }
                
                var data = try handle.read(upToCount: 1)
                
                let filePathResourceValues = try filePath.resourceValues(forKeys: [.fileSizeKey])
                let fileSize = filePathResourceValues.fileSize
                
                var array: [Character] = Array(repeating: "n", count: fileSize!)
                
                var index: Int = 0
                while data != nil && !data!.isEmpty {
                    let symbol: Character = Character(UnicodeScalar(data![0]))
                    if ("0" <= symbol && symbol <= "4") {
                        array[index] = symbol
                        index = index + 1
                    } else if (symbol == "\n") {
                        array[index] = "A"
                        index = index + 1
                    }
                    data = try handle.read(upToCount: 1)
                }

                if (array[index] != "A") {
                    array[index] = "A"
                    index = index + 1
                }
                
                let text = String(array.prefix(index))
                
                array.removeAll()
                
                return text
            } catch {
                print("Contents could not be loaded")
            }
        } else {
            print("File was not found")
        }
        return "Wrong path"
    }
}
