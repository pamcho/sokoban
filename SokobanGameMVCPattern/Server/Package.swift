// swift-tools-version: 5.7
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Server",
    dependencies: [
        .package(url: "https://github.com/Kitura/BlueSocket.git", branch: "master"),
    ],
    targets: [
        .executableTarget(
            name: "Server",
            dependencies: [
                .product(name: "Socket", package: "bluesocket")
            ],
        resources: [
                    .process("levels/level7.sok"),
                    .process("levels/level8.sok"),
                    .process("levels/level9.sok")]),
        .testTarget(
            name: "ServerTests",
            dependencies: ["Server",
            ]),
    ]
)
