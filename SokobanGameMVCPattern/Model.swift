import AVFoundation

class Model {
    
//    MARK: - Properties
    private let viewer: Viewer
    private var stateModel: Bool
    private var desktop: [[Int]]
    private var indexX: Int
    private var indexY: Int
    private var arrayOfIndexies: [[Int]]
    private var levels: Levels
    private var audioPlayer: AVAudioPlayer?
    
//    MARK: - Initializer
    public init(_ viewer: Viewer) {
        self.viewer = viewer
        levels = Levels()
        desktop = levels.nextLevel()
        indexX = 1
        indexY = 1
        stateModel = true
        arrayOfIndexies = []
        initialization()
    }
    
    private func initialization() {
        var countOne: Int = 0
        var countThree: Int = 0
        var countFour: Int = 0
        
        for i in 0..<desktop.count {
            for j in 0..<desktop[i].count {
                if desktop[i][j] == 1 {
                    countOne = countOne + 1
                    self.indexX = j
                    self.indexY = i
                } else if desktop[i][j] == 3 {
                    countThree = countThree + 1
                } else if desktop[i][j] == 4 {
                    countFour = countFour + 1
                }
            }
        }
        
        
        if (countOne != 1 ) {
            stateModel = false
//            desktop = [[Int]]()
            return
        }
        
        if (!(countThree != 0 && countFour != 0 && countFour == countThree)) {
            stateModel = false
            return
        }
        
        arrayOfIndexies = Array(repeating: Array(repeating: 0, count: countFour), count: 2)
        
        var t: Int = 0
        for i in 0..<desktop.count {
            for j in 0..<desktop[i].count {
                if desktop[i][j] == 4 {
                    arrayOfIndexies[0][t] = i
                    arrayOfIndexies[1][t] = j
                    t = t + 1
                }
            }
        }
    }
    
//    MARK: - Getters and Setters
    public func getStateModel() -> Bool {
        return stateModel
    }
    
    public func getDesktop() -> [[Int]] {
        return desktop
    }
    
//    MARK: - Public methods
    public func move(_ direction: String) {
        if (direction == "Right") {
            moveRight()
        } else if (direction == "Left") {
            moveLeft()
        } else if (direction == "Up") {
            moveUp()
        } else if (direction == "Down") {
            moveDown()
        } else {
            return
        }
        check()
        viewer.update()
        won()
    }
    
//    MARK: - Helpers
    private func check() {
        for j in 0..<arrayOfIndexies[0].count {
            let y: Int = arrayOfIndexies[0][j]
            let x: Int = arrayOfIndexies[1][j]
            if (desktop[y][x] == 0) {
                desktop[y][x] = 4
                return
            }
        }
    }
    
    private func won() {
        var won: Bool = true
        for j in 0..<arrayOfIndexies[0].count {
            let y: Int = arrayOfIndexies[0][j]
            let x: Int = arrayOfIndexies[1][j]
            if (desktop[y][x] != 3) {
                won = false
                break
            }
        }
        
        if (won) {
            playSound("win")
            viewer.showMessageDialog()
            desktop = levels.nextLevel()
            
            if desktop.isEmpty {
                viewer.showErrorMessage()
            }
            
            initialization()
            viewer.update()
        }
    }
    
    private func moveUp() {
        var soundAction = "step"
        
        if (desktop[indexY - 1][indexX] == 3) {
           if (desktop[indexY - 2][indexX] == 0 || desktop[indexY - 2][indexX] == 4) {
              desktop[indexY - 1][indexX] = 0
              desktop[indexY - 2][indexX] = 3
               soundAction = "move"
           } else {
               soundAction = "no"
           }
        }
        
        if desktop[indexY - 1][indexX] == 0 || desktop[indexY - 1][indexX] == 4 {
            desktop[indexY][indexX] = 0
            indexY = indexY - 1
            desktop[indexY][indexX] = 1
        } else {
            soundAction = "no"
        }
        playSound(soundAction)
        viewer.update()
    }
    
    private func moveRight() {
        var soundAction = "step"
        
       if (desktop[indexY][indexX + 1] == 3) {
          if (desktop[indexY][indexX + 2] == 0 || desktop[indexY][indexX + 2] == 4) {
             desktop[indexY][indexX + 1] = 0
             desktop[indexY][indexX + 2] = 3
              soundAction = "move"
          } else {
              soundAction = "no"
          }
       }

       if (desktop[indexY][indexX + 1] == 0 || desktop[indexY][indexX + 1] == 4) {
          desktop[indexY][indexX] = 0;
          indexX = indexX + 1;
          desktop[indexY][indexX] = 1;
       } else {
           soundAction = "no"
       }
        playSound(soundAction)
        viewer.update()
    }
    
    private func moveDown() {
        var soundAction = "step"
        
        if (desktop[indexY + 1][indexX] == 3) {
           if (desktop[indexY + 2][indexX] == 0 || desktop[indexY + 2][indexX] == 4) {
              desktop[indexY + 1][indexX] = 0
              desktop[indexY + 2][indexX] = 3
               soundAction = "move"
           } else {
               soundAction = "no"
           }
        }
        
        if desktop[indexY + 1][indexX] == 0 || desktop[indexY + 1][indexX] == 4 {
            desktop[indexY][indexX] = 0
            indexY = indexY + 1
            desktop[indexY][indexX] = 1
        } else {
            soundAction = "no"
        }
        playSound(soundAction)
        viewer.update()
    }
    
    private func moveLeft() {
        var soundAction = "step"
        
        if (desktop[indexY][indexX - 1] == 3) {
           if (desktop[indexY][indexX - 2] == 0 || desktop[indexY][indexX - 2] == 4) {
              desktop[indexY][indexX - 1] = 0
              desktop[indexY][indexX - 2] = 3
               soundAction = "move"
           } else {
               soundAction = "no"
           }
        }

        if (desktop[indexY][indexX - 1] == 0 || desktop[indexY][indexX - 1] == 4) {
           desktop[indexY][indexX] = 0;
           indexX = indexX - 1;
           desktop[indexY][indexX] = 1;
        } else {
            soundAction = "no"
        }
        playSound(soundAction)
         viewer.update()
    }
    
    private func playSound(_ action: String) {
        let url: URL
        
        if action == "win" {
            guard let path = Bundle.main.path(forResource: "sounds/win_sound", ofType: "mp3") else { return }
            url = URL(fileURLWithPath: path)
        } else if action == "move" {
            guard let path = Bundle.main.path(forResource: "sounds/move_block", ofType: "mp3") else { return }
            url = URL(fileURLWithPath: path)
        } else if action == "no" {
            guard let path = Bundle.main.path(forResource: "sounds/no", ofType: "mp3") else { return }
            url = URL(fileURLWithPath: path)
        } else {
            guard let path = Bundle.main.path(forResource: "sounds/walking_sound", ofType: "mp3") else { return }
            url = URL(fileURLWithPath: path)
        }
        
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            audioPlayer?.play()
        } catch {
            print("Sound could not be played")
        }
            
    }
}

