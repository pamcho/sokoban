import UIKit

@main
class Viewer: UIResponder, UIApplicationDelegate {

//    MARK: - Properties
    var window: UIWindow?
    private var canvas: Canvas?
    
//    MARK: - Initializer
    public override init() {
        window = UIWindow(frame: UIScreen.main.bounds)
        super.init()
        let controller: Controller = Controller(self)
        canvas = Canvas(controller.getModel())
        controller.view.addSubview(canvas!)
        window?.contentMode = .scaleToFill
        window?.rootViewController = controller
        controller.view.frame = window?.safeAreaLayoutGuide.layoutFrame ?? .zero
        canvas?.frame = controller.view.frame
        window?.makeKeyAndVisible()
        
    }

//    MARK: - Public methods
    public func update() {
        canvas!.update()
    }
        
    public func showMessageDialog() {
        
        let winAlert = UIAlertController(title: "Congratulations!", message: "You passed the level", preferredStyle: .alert)
        
        winAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.window?.rootViewController?.present(winAlert, animated: true, completion: nil)
    }
    
    public func showErrorMessage() {
        
        let winAlert = UIAlertController(title: "Ooops!", message: "The level could not be loaded", preferredStyle: .alert)
        
        winAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.window?.rootViewController?.present(winAlert, animated: true, completion: nil)
    }
}
